package com.ash.pipedrive.ui



import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.ash.pipedrive.R
import com.ash.pipedrive.adapter.PersonsListAdapter
import com.ash.pipedrive.viewmodel.ListViewModel
import com.ash.pipedrive.viewmodel.ListViewModelFactory
import com.paginate.Paginate
import kotlinx.android.synthetic.main.activity_main.*


class PersonListFragment : Fragment(), Paginate.Callbacks {


    private val personListViewModel: ListViewModel by viewModels {
        ListViewModelFactory(requireContext())
    }

    private var page = 0
    private var isLoading = false
    private var hasLoadedAllItems = false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_person_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribe()
    }

    private fun subscribe() {
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        val adapter = PersonsListAdapter()
        recyclerView.adapter = adapter
        Paginate.with(recyclerView, this).build()
        personListViewModel.pagedListLiveData.observe(this, Observer {
            adapter.submitList(it)
        })
    }

    override fun onLoadMore() {
        if (!isLoading) {
            isLoading = true
            personListViewModel.loadPersons(page++).observe(this, Observer { response ->
                isLoading = false
                hasLoadedAllItems = !response.additional_data.pagination.more_items_in_collection
            })
        }
    }

    override fun isLoading(): Boolean = isLoading

    override fun hasLoadedAllItems(): Boolean = hasLoadedAllItems


}
