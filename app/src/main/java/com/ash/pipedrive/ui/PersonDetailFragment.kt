package com.ash.pipedrive.ui


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.viewModels
import com.ash.pipedrive.Constant
import com.ash.pipedrive.R
import com.ash.pipedrive.model.data.Person
import com.ash.pipedrive.util.CircleTransform
import com.ash.pipedrive.util.GeneralUtil.Companion.toMd5Hash
import com.ash.pipedrive.viewmodel.DetailViewModel
import com.ash.pipedrive.viewmodel.DetailViewModelFactory
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_person_detail.*

private const val PERSON = "Person"

class PersonDetailFragment : Fragment() {

    private var person: Person? = null

    private val personDetailViewModel: DetailViewModel by viewModels {
        DetailViewModelFactory()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            person = it.getParcelable<Person>(PERSON)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_person_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populateUi()
    }

    private fun populateUi() {
        with(person!!) {
            setAvatar(email[0].value, personIcon)
            personName.text = name
            org.text = org_name
            phoneTv.text = phone[0].value
            emailTv.text = email[0].value
        }
    }

    private fun setAvatar(email: String?, imageView: ImageView) {
        val hash = toMd5Hash(email)
        val imageUrl: String = Constant.GRAVATAR_URL + hash + "?&d=monsterid"
        Picasso.get()
            .load(imageUrl)
            .fit().centerCrop()
            .transform(CircleTransform())
            .error(R.drawable.ic_person_black_24dp)
            .into(imageView)
    }


}
