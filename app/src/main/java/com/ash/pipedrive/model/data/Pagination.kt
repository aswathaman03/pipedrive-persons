package com.ash.pipedrive.model.data

data class Pagination(val start: Long, val limit: Long, val more_items_in_collection: Boolean)