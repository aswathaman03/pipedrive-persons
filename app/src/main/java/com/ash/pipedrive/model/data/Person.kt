package com.ash.pipedrive.model.data

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Person(
    @PrimaryKey
    val id: Int,
    val name: String,
    val phone: ArrayList<Contact>,
    val email: ArrayList<Contact>,
    val org_name: String,
    val cc_email: String,
    @TypeConverters(DateConverter::class)
    val update_time: String,
    @TypeConverters(DateConverter::class)
    val add_time: String
) : Parcelable