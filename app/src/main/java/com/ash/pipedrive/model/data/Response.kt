package com.ash.pipedrive.model.data

data class Response(val data: ArrayList<Person>, val additional_data: AdditionalData)