package com.ash.pipedrive.model.data

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Transaction
import com.ash.pipedrive.util.GeneralUtil

@Dao
interface PersonDao {

    @Query("SELECT * FROM person ORDER BY add_time ASC")
    fun selectPaged(): DataSource.Factory<Int, Person>

    @Transaction
    fun updatePersons(
        persons: List<Person>,
        isFirstPage: Boolean,
        hasLoadedAllItems: Boolean
    ) {

        val minAddTime = GeneralUtil.stringToLong(persons.first().add_time)

        val maxAddTime = GeneralUtil.stringToLong(persons.last().add_time)

        if (!isFirstPage)
            deleteRange(minAddTime, maxAddTime)
        else
            deleteRangeFirst(minAddTime)

        insert(persons)
    }

    @Query("DELETE FROM person WHERE add_time BETWEEN :minUpdateTime AND :maxUpdateTime")
    fun deleteRange(minUpdateTime: Long, maxUpdateTime: Long)

    @Query("DELETE FROM person WHERE add_time < :minUpdateTime")
    fun deleteRangeFirst(minUpdateTime: Long)

    @Insert(onConflict = REPLACE)
    fun insert(persons: List<Person>)

}


