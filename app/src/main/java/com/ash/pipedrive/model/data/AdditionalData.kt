package com.ash.pipedrive.model.data

data class AdditionalData (val pagination: Pagination)