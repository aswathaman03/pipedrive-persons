package com.ash.pipedrive.model.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Contact(val label: String, val value: String, val primary: Boolean) : Parcelable