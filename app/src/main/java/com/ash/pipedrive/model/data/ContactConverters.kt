package com.ash.pipedrive.model.data

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ContactConverters {

    @TypeConverter
    fun contactsToString(contacts: ArrayList<Contact>): String {
        val gson = Gson()
        val type = object : TypeToken<ArrayList<Contact>>() {}.type
        return gson.toJson(contacts, type)
    }

    @TypeConverter
    fun stringToContacts(contactString: String): ArrayList<Contact> {
        val type = object : TypeToken<ArrayList<Contact>>() {}.type
        return Gson().fromJson(contactString, type)
    }


}