package com.ash.pipedrive.model.network

import com.ash.pipedrive.model.data.Response
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface RepoService {
    @GET("v1/persons")
    fun getPersons(@Query("start") start: String, @Query("limit") limit: String, @Query("api_token") api_token: String): Observable<Response>
}

