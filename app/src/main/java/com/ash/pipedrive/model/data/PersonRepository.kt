package com.ash.pipedrive.model.data

import androidx.paging.DataSource


class PersonRepository constructor(private val personDao: PersonDao) {
    fun selectPaged(): DataSource.Factory<Int, Person> = personDao.selectPaged()

    fun updatePersons(
        persons: List<Person>,
        isFirstPage: Boolean,
        hasLoadedAllItems: Boolean
    ) = personDao.updatePersons(persons, isFirstPage, hasLoadedAllItems)

}