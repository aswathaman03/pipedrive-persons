package com.ash.pipedrive.model.data

import androidx.room.TypeConverter
import java.text.SimpleDateFormat
import java.util.*

class DateConverter {
    @TypeConverter
    fun stringToLong(date: String): Long {
        val outputFormat = SimpleDateFormat("yyyyMMddHHmmss", Locale.US)
        val inputFormat = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US)
        val dateObj: Date = inputFormat.parse(date)
        val outputString: String = outputFormat.format(dateObj)
        return outputString.toLong()
    }
}