package com.ash.pipedrive.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.ash.pipedrive.Constant
import com.ash.pipedrive.di.DaggerRepositoryModuleInjector
import com.ash.pipedrive.di.DatabaseModule
import com.ash.pipedrive.di.NetworkModule
import com.ash.pipedrive.model.data.Person
import com.ash.pipedrive.model.data.PersonRepository
import com.ash.pipedrive.model.data.Response
import com.ash.pipedrive.model.network.RepoService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ListViewModel(context: Context) : ViewModel() {

    @Inject
    lateinit var repoService: RepoService

    @Inject
    lateinit var personRepository: PersonRepository

    private val compositeDisposable = CompositeDisposable()


    val pagedListLiveData: LiveData<PagedList<Person>> by lazy {
        val dataSourceFactory = personRepository.selectPaged()
        val config = PagedList.Config.Builder()
            .setPageSize(Constant.PAGE_SIZE)
            .setEnablePlaceholders(true)
            .build()
        LivePagedListBuilder(dataSourceFactory, config).build()
    }


    init {
        DaggerRepositoryModuleInjector.builder().networkModule(NetworkModule).databaseModule(DatabaseModule(context))
            .build().inject(this)
    }


    fun loadPersons(page: Int): LiveData<Response> {
        val response = MutableLiveData<Response>()
        with(repoService) {
            getPersons(page.toString(), Constant.PAGE_LIMIT, Constant.API_KEY).concatMap { value ->
                personRepository.updatePersons(
                    value.data,
                    page == 0,
                    !value.additional_data.pagination.more_items_in_collection
                )
                Observable.just(value)
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { disposable -> onSubscribed(disposable) }
                .subscribe({ value ->
                    response.value = value
                }, {

                })
        }
        return response
    }

    private fun onSubscribed(subscription: Disposable) {
        compositeDisposable.add(subscription)
    }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}

