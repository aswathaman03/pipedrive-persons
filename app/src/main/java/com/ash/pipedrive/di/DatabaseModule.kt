package com.ash.pipedrive.di

import android.content.Context
import com.ash.pipedrive.model.data.AppDatabase
import com.ash.pipedrive.model.data.PersonRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule(private var context: Context) {

    @Singleton
    @Provides
    fun provideContext(): Context {
        return context
    }

    @Singleton
    @Provides
    fun providePersonRepository(context: Context): PersonRepository {
        return PersonRepository(
            AppDatabase.getInstance(
                context
            ).personDao()
        )
    }


}