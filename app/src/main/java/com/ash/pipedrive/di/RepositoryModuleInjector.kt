package com.ash.pipedrive.di

import com.ash.pipedrive.viewmodel.ListViewModel
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [(NetworkModule::class),(DatabaseModule::class)])
interface RepositoryModuleInjector {

    fun inject(listViewModel: ListViewModel)

    @Component.Builder
    interface Builder {
        fun build(): RepositoryModuleInjector

        fun databaseModule(databaseModule: DatabaseModule): Builder

        fun networkModule(networkModule: NetworkModule): Builder
    }

}