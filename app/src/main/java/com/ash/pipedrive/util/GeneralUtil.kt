package com.ash.pipedrive.util

import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.*

class GeneralUtil {
    companion object{
        fun stringToLong(date: String): Long {
            val outputFormat = SimpleDateFormat("yyyyMMddHHmmss", Locale.US)
            val inputFormat = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US)
            val dateObj: Date = inputFormat.parse(date)
            val outputString: String = outputFormat.format(dateObj)
            return outputString.toLong()
        }

        fun toMd5Hash(email: String?): String? {
            return try {
                val md5 = MessageDigest.getInstance("MD5")
                val md5HashBytes: Array<Byte> = md5.digest(email?.toByteArray()).toTypedArray()
                byteArrayToString(md5HashBytes)
            } catch (e: Exception) {
                null
            }
        }

        private fun byteArrayToString(array: Array<Byte>): String {
            val result = StringBuilder(array.size * 2)
            for (byte in array) {
                val toAppend: String = String.format("%x", byte).replace(" ", "0")
                result.append(toAppend)
            }
            return result.toString()
        }

    }
}