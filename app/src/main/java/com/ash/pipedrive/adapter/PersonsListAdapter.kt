package com.ash.pipedrive.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ash.pipedrive.R
import com.ash.pipedrive.model.data.Person
import com.ash.pipedrive.ui.PersonListFragmentDirections
import kotlinx.android.synthetic.main.person_item.view.*

class PersonsListAdapter : PagedListAdapter<Person, PersonsListAdapter.ViewHolder>(PersonDiffCallBack()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val person = getItem(position)
        holder.apply {
            bind(createOnClickListener(person!!), person)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.person_item, parent, false)
        return ViewHolder(view)
    }

    private fun createOnClickListener(person: Person): View.OnClickListener {
        return View.OnClickListener {
            val direction = PersonListFragmentDirections.actionPersonListFragmentToPersonDetailFragment(person)
            it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(
        private val view: View
    ) : RecyclerView.ViewHolder(view) {
        fun bind(listener: View.OnClickListener, person: Person) {
            view.setOnClickListener(listener)
            view.personName.text = person.name
        }
    }

}

private class PersonDiffCallBack : DiffUtil.ItemCallback<Person>() {

    override fun areItemsTheSame(oldItem: Person, newItem: Person): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Person, newItem: Person): Boolean {
        return oldItem == newItem
    }

}

