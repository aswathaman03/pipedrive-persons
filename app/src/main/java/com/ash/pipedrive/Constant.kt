package com.ash.pipedrive

class Constant {
    companion object {
        const val PAGE_LIMIT = "5"
        const val PAGE_SIZE = 5
        const val API_KEY = "4522ae07b40517b42b440064dc8ae27308ff555a"
        const val GRAVATAR_URL = "https://www.gravatar.com/avatar/"
    }
}